<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

return new \Phalcon\Config(array(
    'database' => array(
        'adapter'     => 'Mysql',
        'host'        => 'localhost',
        'username'    => 'root',
        'password'    => '',
        'dbname'      => 'test',
        'charset'     => 'utf8',
    ),
    'application' => array(
        'controllersDir' => APP_PATH . '/app/controllers/',
        'modelsDir'      => APP_PATH . '/app/models/',
        'migrationsDir'  => APP_PATH . '/app/migrations/',
        'viewsDir'       => APP_PATH . '/app/views/',
        'pluginsDir'     => APP_PATH . '/app/plugins/',
        'libraryDir'     => APP_PATH . '/app/library/',
        'cacheDir'       => APP_PATH . '/app/cache/',
        'baseUri'        => '/',
    ),
    'mail' => array(
        "from"          =>   "capitancookeventos@gmail.com",
        "fromName"      =>   "Capitan Cook Eventos",
        "host"          =>   "smtp.gmail.com",
        "SMTPAuth"      =>   true,
        "username"      =>   "dd.inspiration.soporte@gmail.com",
        "password"      =>   "2314dani",
        'charset'       =>  'UTF-8',
        "SMTPSecure"    =>   "tls",
        "port"          =>   "587"
    ),
    "recaptcha" => array(
        'publicKey'         => '6Ldz2I4UAAAAACYI-3NVcyJIXedIOv8VtbQ6nY4W',
        'secretKey'         => '6Ldz2I4UAAAAAGo12Q-ceL2PUSWR3P5CKyyWfM3y',
        'jsApiUrl'          => 'https://www.google.com/recaptcha/api.js',
        'verifyUrl'         => 'https://www.google.com/recaptcha/api/siteverify',
    )
));
