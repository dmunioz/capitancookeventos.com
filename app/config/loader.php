<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader
    ->registerDirs(
        array(
            $config->application->controllersDir,
        )
    )
    ->registerNamespaces(
        [
            'Libreria\PHPMailer' => '../app/library/phpmailer/',
            'Libreria\Seguridad' => '../app/library/recaptcha/',
        ]
    )
    ->register();
