<?php
/**
 * Envia correos basado en plantillas predeterminadas
 * User: Daniel
 * Date: 03/02/2019
 * Time: 10:38 AM
 */

namespace Libreria\PHPMailer;

use Phalcon\Mvc\User\Component,
    Phalcon\Mvc\View;

class ServicioEmail extends Component
{
    /**
     * @param $accion
     * @param $params
     * @param $mail
     * @return string
     */
    public function getTemplate($controlador, $accion, $params, $mail, $imagenes = null)
    {
        foreach ($imagenes as $imagen) {
            $mail->AddEmbeddedImage($imagen['path'], $imagen['cid'], $imagen['file']);
        }
        $parameters = array_merge(array(
            'publicUrl' => $this->config->application->publicUrl
//            'publicUrl' => '/capitan/'
        ), $params);
        return $this->view->getRender($controlador, $accion, $parameters, function ($view) {
            $view->setRenderLevel(View::LEVEL_LAYOUT);
        });
//        return $view->getContent();
    }

    /**
     * @param $controlador String
     * @param $accion String Convencion: templateNombre
     * @param $params array parametros a reemplazar en el template
     * @param $addresses String direcciones
     * @param $subject String asunto
     * @param null $imagenes array['path'=>'directorio completo','cid'=>'id del template','file'=>'nombre.extension']
     * @return bool
     */
    public function send($controlador, $accion, $params, $addresses, $subject, $imagenes = array(), $responderA = array(), $bccs = array())
    {
        //obtenemos la instancia de PHPMAILER
        $mail = $this->getDi()->getShared('email');
        //obtenemos la configuración del email app/config/config.php
        $emailConfig = $this->config->mail;
//        $mail->SMTPDebug = 2;


        $mail->isSMTP();
        $mail->Host = $emailConfig->host;
        $mail->SMTPAuth = $emailConfig->SMTPAuth;
        $mail->Username = $emailConfig->username;
        $mail->Password = $emailConfig->password;
        // // $mail->SMTPSecure   = $emailConfig->SMTPSecure;
        $mail->Port = $emailConfig->port;
        foreach ($responderA as $resp) {
//            cliente
            $mail->AddReplyTo($resp['correo'], $resp['nombre']);
        }
        foreach ($bccs as $bcc) {
//        cliente
            $mail->AddBCC($bcc);
        }
//        $mail->From = $emailConfig->from;
        $mail->From = 'capitancookeventos@gmail.com';

//        $mail->FromName = $emailConfig->fromName;
        $mail->FromName = 'Capitan Cook Eventos';
        $mail->CharSet = $emailConfig->charset;

        $mail->addAddress($addresses, 'Capitan Cook');
        //$message = file_get_contents('$controlador');
        //$message = str_replace('%p%', "DMUNIOZ", $message);
        //$mail->MsgHTML($message);

        $mail->isHTML(true);

        $mail->Subject = $subject;
        $path = 'images/general/logo.jpg';
        $cid = md5($path);
        $mail->AddEmbeddedImage($path, $cid, 'ImmagineLogo');
//        $mail->Body = $this->getTemplate($controlador, $accion, $params, $mail, $imagenes);
        $mail->Body = $this->Mensaje($params, $cid);

//        if (!$mail->send()) {
//            return $mail->ErrorInfo;
//        } else {
//            return true;
//        }
        if (!$mail->send()) {
            $output = array('type' => 'error',
                'text' => 'No se pudo entregar el correo. Intentelo nuevamente',
                'body' => $mail->Body
            );
            echo json_encode($output);
            return;
        } else {
            $output = array('type' => 'message', 'text' => 'Gracias ! Tu mensaje ha sido entregado correctamente (verifica tu casilla de correo no deseados).');
            echo json_encode($output);
            return;
        }
    }

    public function Mensaje($params, $cid)
    {
        return "   
        <table align=\"center\" bgcolor=\"black\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"tableContent\" width=\"100%\">
    <tbody>
    <tr>
        <td>
            <table align=\"center\" bgcolor=\"#f6f6f6\" border=\"0\" cellspacing=\"0\"
                   style=\" background-color: #272727; color: white; font-family:helvetica, sans-serif;\" width=\"600\" class=\"MainContainer\">
                <tbody>
                <tr>
                    <td>
                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\" background-color: #272727; color: white;\">
                            <tbody>
                            <tr>
                                <td valign=\"top\" width=\"20\">&nbsp;</td>
                                <td valign=\"top\">
                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">

                                        <!--  =========================== The header ===========================  -->
                                        <tbody>
                                        <tr>
                                            <!--  =========================== The body ===========================  -->
                                            <td class=\"movableContentContainer\">
                                                <div class=\"movableContent\"
                                                     style=\"border: 0px; padding-top: 0px; position: relative;\">
                                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                                                        <tr>
                                                            <td height=\"25\">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table align=\"center\" border=\"0\" cellpadding=\"0\"
                                                                       cellspacing=\"0\" width=\"100%\">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td align=\"left\" valign=\"middle\">
                                                                            <div class=\"contentEditableContainer contentImageEditable\">
                                                                                <div class=\"contentEditable\">
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td align=\"right\" valign=\"top\">
                                                                            <div class=\"contentEditableContainer contentImageEditable\"
                                                                                 style=\"display:inline-block;\">
                                                                                <div class=\"contentEditable\">
                                                                                    Por favor no responda este correo.</div>
                                                                            </div>
                                                                        </td>
                                                                        <td align=\"right\" valign=\"top\" width=\"18\">
                                                                            <div class=\"contentEditableContainer contentImageEditable\"
                                                                                 style=\"display:inline-block;\">
                                                                                <div class=\"contentEditable\">
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class=\"movableContent\"
                                                     style=\"border: 0px; padding-top: 0px; position: relative;\">
                                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                                                        <tr>
                                                            <td height=\"25\">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align=\"center\" bgcolor=\"#000000\" height=\"184\"
                                                                style=\"background-size:560px 184px;\" valign=\"middle\"
                                                                width=\"560\">
                                                                <div class=\"contentEditableContainer contentImageEditable\">
                                                                    <div class=\"contentEditable\">
                                                                        <img  src='http://www.capitancookeventos.com/images/general/capitan.png',
                                                                        alt='logo' width='320'/>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class=\"movableContent\"
                                                     style=\"border: 0px; padding-top: 0px; position: relative;\">
                                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                                                        <tr>
                                                            <td align=\"right\" bgcolor=\"#008200\"
                                                                style=\"border-top: 2px solid white;padding:20px;\">
                                                                <div class=\"contentEditableContainer contentTextEditable\">
                                                                    <div class=\"contentEditable\">
                                                                        <p style=\"color:#ffffff;font-size:16px;line-height:19px;\">
                                                                             CONSULTA WEB</p>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class=\"movableContent\"
                                                     style=\"border: 0px; padding-top: 0px; position: relative;\">
                                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                                                        <tbody>
                                                        <tr>
                                                            <td height=\"25\">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table align=\"center\" class='bgItem' border=\"0\"
                                                                       cellpadding=\"0\" cellspacing=\"0\"
                                                                       style=\"margin-bottom:20px; border:0px solid #dddddd\" valign=\"top\"
                                                                       width=\"100%\">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td colspan=\"3\" height=\"16\">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width=\"16\">
                                                                        </td>
                                                                        <td>
                                                                            <table align=\"center\" class='bgItem'
                                                                                   border=\"0\" cellpadding=\"0\"
                                                                                   cellspacing=\"0\" valign=\"top\"
                                                                                   width=\"100%\">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td align=\"left\" valign=\"top\">
                                                                                        <div class=\"contentEditableContainer contentTextEditable\">
                                                                                            <div class=\"contentEditable\">
                                                                                                <h2 style='color: white !important;'> 
                                                                                                 Información:
                                                                                                </h2>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height=\"13\">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align=\"left\" valign=\"top\">
                                                                                        <div class=\"contentEditableContainer contentTextEditable\">
                                                                                            <div class=\"contentEditable\" style='color: white !important;'>
                                                                                            $params
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height=\"20\">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align=\"right\"
                                                                                        style=\"padding-bottom:8px;\"
                                                                                        valign=\"top\">
                                                                                        <div class=\"contentEditableContainer contentTextEditable\">
                                                                                            <div class=\"contentEditable\">
                                                                                              
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td width=\"16\">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan=\"3\" height=\"16\">
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>


                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td valign=\"top\" width=\"20\">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>";
    }
}