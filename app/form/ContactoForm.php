<?php

//namespace Formulario;

/**
 * Created by PhpStorm.
 * User: dmunioz
 * Date: 17/09/2015
 * Time: 13:34
 */
//use Libreria\Seguridad\ReCaptcha as Recaptcha;
//use Libreria\Seguridad\RecaptchaValidator as RecaptchaValidator;

class ContactoForm extends \Phalcon\Forms\Form
{

    public function initialize($entity = null, $options = array())
    {

        $recaptcha = new Recaptcha('recaptcha', array('required' => '', 'class' => 'g-recaptcha'));
        $recaptcha->addValidator(new RecaptchaValidator(array(
            'message' => "Es usted humano? <strong>Complete el CAPTCHA.</strong> "
        )));

        $this->add($recaptcha);


    }

    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        $cadena = "";
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                //$this->flash->error($message);
                $cadena .= $message . "<br>";//para mostrar con tooltip
            }
        }
        return $cadena;
    }
}