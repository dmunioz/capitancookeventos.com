<!-- Home Section -->
<div id="galeria" class="relative">
    {#<a class="btn_galeria btn btn-mod btn-w btn-round"#}
    {#style="">#}
    {#<i class="fa fa-image"></i>#}
    {#Ver galería completa#}
    {#</a>#}
    <!-- Fullwidth Slider -->
    <div id="carousel" class="home-section fullwidth-slideshow black-arrows bg-dark">


        <!-- Slide Item -->
        <section class="home-section bg-scroll bg-dark" data-background="images/galeria/01.jpg">
            <div class="js-height-full container">
            </div>
        </section>
        <!-- End Slide Item -->

        <!-- Slide Item -->
        <section class="home-section bg-scroll bg-dark" data-background="images/galeria/02.jpg">
            <div class="js-height-full container">
            </div>
        </section>
        <!-- End Slide Item -->

        <!-- Slide Item -->
        <section class="home-section bg-scroll bg-dark" data-background="images/galeria/03.jpg">
            <div class="js-height-full container">
            </div>
        </section>
        <!-- End Slide Item -->

        <!-- Slide Item -->
        <section class="home-section bg-scroll bg-dark" data-background="images/galeria/04.jpg">
            <div class="js-height-full container">
            </div>
        </section>
        <!-- End Slide Item -->

        <!-- Slide Item -->
        <section class="home-section bg-scroll bg-dark" data-background="images/galeria/05.jpg">
            <div class="js-height-full container">
            </div>
        </section>
        <!-- End Slide Item -->

        <!-- Slide Item -->
        <section class="home-section bg-scroll bg-dark" data-background="images/galeria/06.jpg">
            <div class="js-height-full container">
            </div>
        </section>
        <!-- End Slide Item -->

        <!-- Slide Item -->
        <section class="home-section bg-scroll bg-dark" data-background="images/galeria/07.jpg">
            <div class="js-height-full container">
            </div>
        </section>
        <!-- End Slide Item -->

        <!-- Slide Item -->
        <section class="home-section bg-scroll bg-dark" data-background="images/galeria/08.jpg">
            <div class="js-height-full container">
            </div>
        </section>
        <!-- End Slide Item -->

        <!-- Slide Item -->
        <section class="home-section bg-scroll bg-dark" data-background="images/galeria/09.jpg">
            <div class="js-height-full container">
            </div>
        </section>
        <!-- End Slide Item -->

    </div>

    <!-- End Fullwidth Slider -->

    <!-- Pager Carousel -->
    <div class="fullwidth-slideshow-pager-wrap">
        <div class="container">
            <div class="row">

                <div align="center">
                    {{ link_to('index/portfolio',' <i class="fa fa-image"></i>
                        Ver galería completa','class':'btn btn-mod btn-round') }}
                </div>

                <div class="col-md-8 col-md-offset-2">
                    <div class="fullwidth-slideshow-pager">

                        <!-- Item -->
                        <div class="fsp-item">
                            {{ image('images/galeria/01.jpg', 'alt':'galeria') }}
                        </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="fsp-item">
                            {{ image('images/galeria/02.jpg', 'alt':'galeria') }}
                        </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="fsp-item">
                            {{ image('images/galeria/03.jpg', 'alt':'galeria') }}
                        </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="fsp-item">
                            {{ image('images/galeria/04.jpg', 'alt':'galeria') }}
                        </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="fsp-item">
                            {{ image('images/galeria/05.jpg', 'alt':'galeria') }}
                        </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="fsp-item">
                            {{ image('images/galeria/06.jpg', 'alt':'galeria') }}
                        </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="fsp-item">
                            {{ image('images/galeria/07.jpg', 'alt':'galeria') }}
                        </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="fsp-item">
                            {{ image('images/galeria/08.jpg', 'alt':'galeria') }}
                        </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="fsp-item">
                            {#<img src="images/photographer/photographer-8-thumb.jpg" alt=""/>#}
                            {{ image('images/galeria/09.jpg', 'alt':'galeria') }}
                        </div>
                        <!-- End Item -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Pager Carousel -->

</div>
<!-- End Home Section -->