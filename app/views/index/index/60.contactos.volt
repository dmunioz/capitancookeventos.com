<!-- Contact Section -->
<section class="page-section bg-dark-alfa-90" id="contactos">
    <div class="pattern"></div>
    <div class="container relative">

        <h2 class="section-title font-alt mb-70 mb-sm-40">
            Contacto
        </h2>

        <div class="row mb-60 mb-xs-40">

            <div class="col-md-8 col-md-offset-2">
                <div class="row">

                    <!-- Phone -->
                    <div class="col-sm-6 col-lg-4 pt-20 pb-20 pb-xs-0">
                        <div class="contact-item">
                            <div class="ci-icon">
                                <i class="fa fa-phone texto_verde_claro"></i>
                            </div>
                            <div class="ci-title font-alt white">
                                LLámanos o escríbenos
                            </div>
                            <div class="ci-text white">
                                +54 9 299 6560957
                            </div>
                        </div>
                    </div>
                    <!-- End Phone -->

                    <!-- Address -->
                    <div class="col-sm-6 col-lg-4 pt-20 pb-20 pb-xs-0">
                        <div class="contact-item">
                            <div class="ci-icon">
                                <i class="fa fa-map-marker texto_verde_claro"></i>
                            </div>
                            <div class="ci-title font-alt white">
                                Dirección
                            </div>
                            <div class="ci-text white">
                                Jubilados Neuquinos 580
                                Ciudad de Neuquén
                            </div>
                        </div>
                    </div>
                    <!-- End Address -->

                    <!-- Email -->
                    <div class="col-sm-6 col-lg-4 pt-20 pb-20 pb-xs-0">
                        <div class="contact-item">
                            <div class="ci-icon">
                                <i class="fa fa-envelope texto_verde_claro"></i>
                            </div>
                            <div class="ci-title font-alt white">
                                Email
                            </div>
                            <div class="ci-text white">
                                <a href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=capitancookeventos@gmail.com"
                                   target="_blank" class="white">capitancookeventos@gmail.com</a>
                            </div>
                        </div>
                    </div>
                    <!-- End Email -->

                </div>
            </div>

        </div>

        <!-- Contact Form -->
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                {#{{ form('enviar',"id":"contact_form","class":"form contact-form","method":"post") }}#}

                <form class="form contact-form" id="contact_form">
                    <div class="clearfix">

                        <div class="cf-left-col">

                            <!-- Name -->
                            <div class="form-group">
                                <input type="text" name="nombre" id="nombre" class="input-md round form-control"
                                       placeholder="Nombre" pattern=".{3,100}" required>
                            </div>

                            <!-- Email -->
                            <div class="form-group">
                                <input type="text" name="email" id="email" class="input-md round form-control"
                                       placeholder="Email" pattern=".{5,100}" required>
                            </div>

                            <!-- Name -->
                            <div class="form-group">
                                <select id="tipo" name="tipo" class="input-md round form-control decorated">
                                    <option value="CUMPLEAÑOS">Cumpleaños</option>
                                    <option value="BODAS">Boda</option>
                                    <option value="EMPRESAS Y COMERCIO">Empresas y Comercio</option>
                                    <option value="INFANTILES">Infantiles</option>
                                    <option value="OTROS EVENTOS">Otros</option>
                                </select>

                            </div>

                            <!-- Email -->
                            <div class="form-group">
                                <input type="number" name="telefono" id="telefono" class="input-md round form-control"
                                       placeholder="Teléfono" pattern=".{5,100}" required>
                            </div>
                        </div>

                        <div class="cf-right-col">

                            <!-- Message -->
                            <div class="form-group">
                                <textarea name="mensaje" id="mensaje" class="input-md round form-control"
                                          style="height: 180px;" placeholder="Mensaje" maxlength="300"></textarea>
                            </div>

                        </div>

                    </div>

                    <div class="clearfix">

                        <div class="cf-left-col">
                            <div class="align-center pt-10">
                                <div class="g-recaptcha" data-sitekey="6Ldz2I4UAAAAACYI-3NVcyJIXedIOv8VtbQ6nY4W"></div>
                                {#{{ contacto.render('recaptcha') }}#}
                            </div>

                            <!-- Inform Tip -->
                            <div class="form-tip pt-20">
                                <i class="fa fa-info-circle"></i> Todos los campos son requeridos
                            </div>

                        </div>
                        <!-- Send Button -->

                        <div class="cf-right-col">


                            <!-- Send Button -->
                            <div class="align-right pt-10">
                                <button class="submit_btn btn btn-mod btn-medium btn-round" id="submit_btn">Enviar Mensaje</button>
                                <button class="submit_btn btn btn-mod btn-medium btn-round" id="submit_btn_espera" style="display: none">Espere un momento por favor...</button>
                            </div>

                        </div>

                    </div>



                    <div id="result"></div>
                </form>

            </div>
        </div>
        <!-- End Contact Form -->

    </div>
</section>
<!-- End Contact Section -->