<!-- Foter -->
<footer class="page-section bg-dark footer pb-60">
    <div class="pattern"></div>
    <div class="container">

        <!-- Footer Logo -->
        <div class="local-scroll mb-30 wow fadeInUp" data-wow-duration="1.2s">
            <a href="#top">
                {{ image('images/general/logo.png', 'width':170, 'height': 55 , 'alt':'logo') }}</a>
        </div>
        <!-- End Footer Logo -->

        <!-- Social Links -->
        <div class="footer-social-links mb-110 mb-xs-60 bg-dark-alfa-30">
            <a href="https://m.me/785330554890487?fbclid=IwAR2eBZkMWOVi0amDd0QeUAL6QFnumI4v3i88761DOIkO6ZvW7B-j5Sh7FCI"
               title="Facebook" target="_blank"
               class=""><i class="fa fa-facebook"></i>
            </a>
            <a href="https://www.instagram.com/capitancook.nqn/" title="Instagram" target="_blank"
               class=""><i
                        class="fa fa-instagram"></i>
            </a>
            <a href="https://open.spotify.com/user/capit%C3%A1ncook?si=G081uupZSqGeS4pyRDJciQ" title="Spotify"
               target="_blank" class=""><i class="fa fa-spotify"></i>
            </a>
            <a href="https://api.whatsapp.com/send?phone=5492996560957" title="Whatsapp" target="_blank"
               class="">
                <i class="fa fa-whatsapp"></i>
            </a>
            <a href="mailto:capitancookeventos@gmail.com" title="Correo" target="_blank"
               class=""><i class="fa fa-envelope"></i></a>
        </div>
        <!-- End Social Links -->

        <!-- Footer Text -->
        <div class="footer-text">

            <!-- Copyright -->
            <div class="footer-copy font-alt">
                <a>&copy; DD Inspiration</a>.
            </div>
            <!-- End Copyright -->

            <div class="footer-made">
            </div>

        </div>
        <!-- End Footer Text -->

    </div>


    <!-- Top Link -->
    <div class="local-scroll">
        <a href="#top" class="link-to-top"><i class="fa fa-caret-up white"></i></a>
    </div>
    <!-- End Top Link -->

</footer>
<!-- End Foter -->