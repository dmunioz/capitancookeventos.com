<!-- Blog Section -->
<section class="page-section bg-dark-alfa-90" id="blog">
    <div class="pattern"></div>

    <div class="container relative">

        <h2 class="section-title font-alt align-left mb-70 mb-sm-40">
            Últimas Entradas

            <a href="https://capitancookeventos.blogspot.com" target="_blank" class="section-more right">Todas las novedades en nuestro blog <i
                        class="fa fa-angle-right"></i></a>

        </h2>

        <div class="row multi-columns-row">

            <!-- Post Item -->
            <div class="col-sm-6 col-md-4 col-lg-4 mb-md-50 wow fadeIn" data-wow-delay="0.1s" data-wow-duration="2s">

                <div class="post-prev-img">
                    <a>
                        {{ link_to('blog/index', image('images/blog/blog02.jpg','alt':'blog', 'class':'')) }}
                    </a>
                </div>

                <div class="post-prev-title font-alt">
                    <a href="">Cómo organizo mi evento paso a paso</a>
                </div>

                <div class="post-prev-info font-alt">
                    <a href="">Facundo Mansegosa</a> | 01 Febrero
                </div>

                <div class="post-prev-text">
                    10 tips del equipo de Capitán Cook para no ser víctima del desorden y el estrés.
                </div>

                {#<div class="post-prev-more">#}
                    {#<a href="" class="btn btn-mod btn-gray btn-round">Seguir leyendo<i#}
                                {#class="fa fa-angle-right"></i></a>#}
                {#</div>#}

            </div>
            <!-- End Post Item -->

            <!-- Post Item -->
            <div class="col-sm-6 col-md-4 col-lg-4 mb-md-50 wow fadeIn" data-wow-delay="0.2s" data-wow-duration="2s">

                <div class="post-prev-img">
                    {{ link_to('blog/index', image('images/blog/blog01.jpg','alt':'blog', 'class':'')) }}

                </div>

                <div class="post-prev-title font-alt">
                    <a href="">¿Qué es un evento?</a>
                </div>

                <div class="post-prev-info font-alt">
                    <a href="">Facundo Mansegosa</a> | 01 Enero
                </div>

                <div class="post-prev-text">
                    Trataremos de definir que es un evento y todos los tipos de eventos que organizamos y hemos organizado.
                </div>

                <div class="post-prev-more">
                    <a href="https://capitancookeventos.blogspot.com/2019/02/que-es-un-evento.html" target="_blank"
                       class="btn btn-mod btn-gray btn-round">Seguir leyendo<i
                                class="fa fa-angle-right"></i></a>
                </div>

            </div>
            <!-- End Post Item -->


        </div>

    </div>
</section>
<!-- End Blog Section -->