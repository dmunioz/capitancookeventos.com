<!-- About Section -->
<style>
    .girar_izq {
        transform: skew(0deg, -7deg);

    }

    .girar_der {
        transform: skew(0deg, 7deg);

    }

    .nosotros_cuadro {
        background-color: #1d1d1d;
        color: #fff;
        padding: 20px;
    }
</style>
<section id="nosotros" class="page-section bg-dark-alfa-90">
    <div class="pattern_2">
    </div>
    <div class="relative">

        <div class="container">
            <div class="">
                    <h2 class="  section-title font-alt mb-70 mb-sm-40">
                        {#CAPITÁN COOK#}
                        QUIÉNES SOMOS?
                    </h2>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">

                        <div class=" nosotros_cuadro section-text align-center mb-10 mb-xs-40"
                             style="color: white">
                            Somos un equipo de jóvenes, apasionados por lo que hacemos. Nuestro objetivo es brindarte
                            todos
                            los servicios que tu evento necesita, en un solo lugar, para que puedas ahorrar tiempo y
                            dinero.
                            Combinamos la buena atención y la calidad de nuestros productos y servicios. Vivimos junto a
                            vos
                            el evento dando lo mejor de nosotros para que nada falle, supervisando todos los detalles
                            para
                            que todo sea perfecto y <br><strong>PUEDAS DISFRUTARLO SIN PREOCUPACIONES</strong>.
                        </div>
                        <div class=" nosotros_cuadro section-text align-center mb-20 mb-xs-40"
                             style="color: white">

                            Nos encargamos de la planificación, organización y coordinación total o parcial del evento,
                            para
                            que sea original, completo y divertido. Escuchamos las necesidades de nuestros clientes y
                            diseñamos la fiesta acorde a sus peticiones y presupuesto.
                            <br><br>
                            Realizamos nuestro trabajo tratando de brindar la mayor confianza desde el primer momento.
                            Con
                            responsabilidad y pasión, guiados por el compromiso, la colaboración, el trabajo en equipo y
                            el
                            respeto.
                        </div>

                        <div class="  align-center">
                            {{ image('images/general/logo_transparente.png', 'width':170, 'height': 55 , 'alt':'logo',
                            'class':'') }}
                        </div>

                    </div>
                </div>
            </div>


        </div>

    </div>
</section>
<!-- End About Section -->