<style>
    .display_none {
        display: none;
    }

    .fixwidth .featherlight-content {
        padding: 25px;
        color: #fff;
        background: #111;
    }
</style>
{#-------------------------------------------#}
<div class="display_none modal_servicios" id="catering">
    <div class="hidden-xs col-xs-12 hidden-sm col-md-4 col-lg-4  mt-30" >
        {{ image('images/servicios/01.jpg', 'alt':'servicio de catering', 'class':'img-responsive') }}
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <h2 class="uppercase">Catering</h2>

        <p>Cada evento es una situación única e irrepetible, y por ello, evaluamos cada caso teniendo en cuenta el
            motivo
            del mismo, el lugar elegido, los tiempos planeados, y los gustos de los invitados.
        </p>

        <p> En función de estas variables y de lo que nuestros clientes nos expresan, armamos nuestra propuesta de
            servicio,
            que luego presentamos a los interesados para analizarla en conjunto y definir el menú final a la medida de
            cada
            evento.
        </p>
    </div>
</div>
{#-------------------------------------------#}
<div class="display_none modal_servicios" id="ambientacion">
    <div class="hidden-xs col-xs-12 hidden-sm col-md-4 col-lg-4  mt-30" >
        {{ image('images/servicios/02.jpg', 'alt':'servicio de ambientación', 'class':'img-responsive') }}
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <h2 class="uppercase">Ambientación</h2>

        <p>Estamos atentos a cada detalle, desde el primero al último, para que usted y sus invitados disfruten de cada
            espacio, detalle y emoción generada con nuestros eventos. </p>

        <p>
            Nuestra intención es ofrecerles a nuestros clientes una manera más eficiente, conveniente y creativa de
            llevar a cabo la ambientación de sus eventos.
        </p>
    </div>
</div>
{#-------------------------------------------#}
<div class="display_none modal_servicios" id="barra_tragos">
    <div class="hidden-xs col-xs-12 hidden-sm col-md-4 col-lg-4  mt-30" >
        {{ image('images/servicios/03.jpg', 'alt':'servicio de tragos', 'class':'img-responsive') }}
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <h2 class="uppercase">Barra de tragos</h2>

        <p>
            Trabajamos una gran diversidad de marcas y tragos según el servicio que usted desee. Hacemos fuerte hincapié
            en la calidad de nuestros insumos y atención de nuestros barmans.
        </p>

        <p> Para que tu fiesta sea ideal, el servicio de barras, no puede fallar. Nuestro servicio más popular es el de
            la barra libre, por ser un servicio sin restricciones y SIN CORTES.
        </p>
    </div>
</div>
{#-------------------------------------------#}
<div class="display_none modal_servicios" id="sonido_iluminacion">
    <div class="hidden-xs col-xs-12 hidden-sm col-md-4 col-lg-4  mt-30" >
        {{ image('images/servicios/04.jpg', 'alt':'servicio de sonido e iluminación', 'class':'img-responsive') }}
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <h2 class="uppercase">Sonido e Iluminación</h2>

        <p>
            Creemos que son piezas fundamentales la calidad del sonido, el tipo de música requerido y la combinación de
            nuestros equipos de iluminación led.
        </p>

        <p>
            Para que la fiesta sea un éxito, necesitas tener la mejor música.
        </p>
    </div>
</div>
{#-------------------------------------------#}
<div class="display_none modal_servicios" id="mozos">
    <div class="hidden-xs col-xs-12 hidden-sm col-md-4 col-lg-4  mt-30" >
        {{ image('images/servicios/05.jpg', 'alt':'servicio de mozos', 'class':'img-responsive') }}
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <h2 class="uppercase">Mozos</h2>

        <p>
            Contamos con los elementos necesarios para implementar cada evento: vajilla completa, mantelería,
            servilletas, cubre sillas y un equipo de mozos cuyo objetivo es brindar la mejor y más eficiente atención a
            los invitados del evento.
        </p>

    </div>
</div>
{#-------------------------------------------#}
<div class="display_none modal_servicios" id="vajilla_manteleria">
    <div class="hidden-xs col-xs-12 hidden-sm col-md-4 col-lg-4  mt-30" >
        {{ image('images/servicios/06.jpg', 'alt':'servicio de vajilla', 'class':'img-responsive') }}
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <h2 class="uppercase">Vajilla y mantelería</h2>

        <p>
            Contamos con los elementos necesarios para implementar cada evento: vajilla completa, mantelería,
            servilletas, cubre sillas y un equipo de mozos cuyo objetivo es brindar la mejor y más eficiente atención a
            los invitados del evento.
        </p>

    </div>
</div>
{#-------------------------------------------#}
<div class="display_none modal_servicios" id="fotografia">
    <div class="hidden-xs col-xs-12 hidden-sm col-md-4 col-lg-4  mt-30" >
        {{ image('images/servicios/07.jpg', 'alt':'servicio de fotografía', 'class':'img-responsive') }}
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <h2 class="uppercase">Fotografía</h2>

        <p>
            Que los mejores momentos de tu evento queden inmortalizados.
        </p>
    </div>
</div>
{#-------------------------------------------#}
<div class="display_none modal_servicios" id="invitaciones">
    <div class="hidden-xs col-xs-12 hidden-sm col-md-4 col-lg-4  mt-30" >
        {{ image('images/servicios/08.jpg', 'alt':'servicio de invitacion', 'class':'img-responsive') }}
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <h2 class="uppercase">Invitaciones</h2>

        <p>
            Tus invitados estarán interesados en tu evento desde el primer momento.
        </p>
    </div>
</div>
{#-------------------------------------------#}
<div class="display_none modal_servicios" id="mesa_dulce_torta">
    <div class="hidden-xs col-xs-12 hidden-sm col-md-4 col-lg-4  mt-30" >
        {{ image('images/servicios/09.jpg', 'alt':'servicio de mesa dulce', 'class':'img-responsive') }}
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <h2 class="uppercase">Mesa dulce y torta</h2>

        <p>
            Agasaja a tus invitados con toda la dulzura de nuestras mesas dulces.
        </p>
    </div>
</div>