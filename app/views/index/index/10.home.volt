{#<!-- Home Section -->#}
<style>


    .slider_h1 {
        font-family: 'BeautifulBloom-Regular', serif !important;
        /*font-size: 44px; font-style: normal; font-variant: normal; font-weight: 700; line-height: 48.4px; */
        text-shadow: -4px -2px 2px #030A0F;
        letter-spacing: 0;
        text-transform: initial;

    }

    .padding_left_1em {
        padding-left: 1em !important;
    }

    .padding_left_2em {
        padding-left: 2em !important;
    }

    .padding_rigth_1em {
        padding-right: 1em !important;
    }

    .padding_rigth_2em {
        padding-right: 2em !important;
    }

    .mini_titulo {
        font-size: 70px;
    }

    .posicion_empresas {
        margin-top: 50px;

    }
    .posicion_logo{
        padding: 0;
    }

    @media only screen and (max-width: 1200px) {
        .posicion_empresas {
            margin-top: 90px !important;

        }

        .mini_titulo {
            font-size: 70px !important;
            letter-spacing: 0.195em;
        }
    }

    @media only screen and (max-width: 767px) {
        .posicion_empresas {
            margin-top: 70px !important;
        }

        .posicion_empresas_sub {
            margin-top: 20em !important;
        }

        .mini_titulo {
            font-size: 40px !important;
            letter-spacing: 0.1em;
        }
        .posicion_logo{
            padding: 5px !important;
        }
    }

    @media only screen and (max-width: 480px) {
        .posicion_empresas {
            margin-left: 10px !important;
            margin-top: 30px !important;
        }

        .posicion_empresas_sub {
            margin-top: 15em !important;
        }

        /*Phone*/
        .mini_titulo {
            font-size: 20px !important;
            letter-spacing: 0.1em;
        }
        .posicion_logo{
            padding: 10px !important;
        }
        .slider_h1{
            text-shadow: -1px -2px 0px #030A0F !important;
        }
    }


</style>
<div class="page-section fullscreen-container" id="home">
    <div class="fullscreenbanner bg-dark">
        <ul>
            <!-- Slide Item -->
            <li data-transition="fade" data-slotamount="7" data-title="Organización de Eventos">

                <img src="images/home/001.jpg?Version1" alt="">


                {#<div class="caption customin customout tp-resizeme  no-transp font-alt "#}
                <div class="caption  customin customout tp-resizeme hs-line-13 no-transp "
                     data-x="right"
                     data-hoffset="0"
                     data-y="center"
                     data-voffset="-100"
                     data-customin="x:-50;y:-300;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"

                     data-speed="800"
                     data-start="800"
                     data-startslide="1"

                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn">

                    <span class="slider_h1 titulo_slider padding_rigth_2em">  Tenemos todo lo que</span>

                </div>


                <div class=" caption customin customout tp-resizeme hs-line-13 pt-40"
                     data-x="right"
                     data-hoffset="0"
                     data-y="center"
                     data-voffset="-14"
                     data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1200"
                     data-startslide="1"

                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn">


                    <span class="slider_h1 titulo_slider padding_rigth_1em"> Tu evento necesita</span>

                </div>


                {#<div class="caption customin customout tp-resizeme"#}
                     {#data-x="center"#}
                     {#data-hoffset="0"#}
                     {#data-y="center"#}
                     {#data-voffset="83"#}
                     {#data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"#}
                     {#data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"#}
                     {#data-speed="800"#}
                     {#data-start="1500"#}
                     {#data-startslide="1"#}

                     {#data-easing="Power4.easeOut"#}
                     {#data-endspeed="500"#}
                     {#data-endeasing="Power4.easeIn">#}

                    {#<div class="local-scroll">#}

                        {#<a href="#about" class="btn btn-mod btn-border-w btn-medium btn-round">#}
                        {#See More#}
                        {#</a>#}

                        {#<span>&nbsp;</span>#}

                        {#<a href="http://vimeo.com/50201327" class="btn btn-mod btn-border-w btn-medium btn-round lightbox mfp-iframe">Play Reel</a>#}

                    {#</div>#}

                {#</div>#}
                {#<div class="caption customin customout tp-resizeme posicion_logo"#}
                     {#data-x="right"#}
                     {#data-hoffset="0"#}
                     {#data-y="bottom"#}
                     {#data-voffset="-20"#}
                     {#data-hoffset="-20"#}
                     {#data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"#}
                     {#data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"#}
                     {#data-speed="800"#}
                     {#data-start="1800"#}
                     {#data-startslide="1"#}

                     {#data-easing="Power4.easeOut"#}
                     {#data-endspeed="500"#}
                     {#data-endeasing="Power4.easeIn"#}
                     {#data-autoplay="false"#}
                     {#data-autoplayonlyfirsttime="false">#}

                    {#<iframe src="http://player.vimeo.com/video/56152991?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="480" height="275" allowfullscreen>#}
                    {#</iframe>#}
                    {#{{ image('images/general/logo_transparente.png','alt':'logo','class':'img-fluid','width':"200") }}#}

                {#</div>#}

            </li>
            <!-- End Slide Item -->


            <!-- Slide Item -->
            <li data-transition="fade" data-slotamount="7" data-title="Cumpleaños">

                <img src="images/home/002.jpg?Version1" alt="">


                <div class="caption customin customout tp-resizeme  no-transp "
                     data-x="left"
                     data-hoffset="0"
                     data-y="center"
                     data-voffset="-100"
                     data-customin="x:-50;y:-300;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"

                     data-speed="800"
                     data-start="1000"
                     data-startslide="1"

                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn">

                    <span class="slider_h1 titulo_slider padding_left_1em"> Cumple   </span>
                    {#We are just creative people#}
                    {#Un evento que merece ser recordado#}
                </div>

                <div class="slider_h1 caption customin customout tp-resizeme  pt-40 "
                     data-x="left"
                     data-hoffset="0"
                     data-y="center"
                     data-voffset="0"
                     data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1300"
                     data-startslide="1"

                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn">

                    {#Design Lovers#}
                    <span class="slider_h1 titulo_slider padding_left_2em">  Quince  </span>

                </div>


                {#<div class="caption customin customout tp-resizeme"#}
                     {#data-x="center"#}
                     {#data-hoffset="0"#}
                     {#data-y="center"#}
                     {#data-voffset="120"#}
                     {#data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"#}
                     {#data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"#}
                     {#data-speed="800"#}
                     {#data-start="1500"#}
                     {#data-startslide="1"#}

                     {#data-easing="Power4.easeOut"#}
                     {#data-endspeed="500"#}
                     {#data-endeasing="Power4.easeIn">#}

                    {#<div class="local-scroll">#}

                        {#<a href="#about" class="btn btn-mod btn-border-w btn-medium btn-round">#}
                        {#See More#}
                        {#</a>#}

                        {#<span>&nbsp;</span>#}

                        {#<a href="pages-pricing-1.html" class="btn btn-mod btn-border-w btn-medium btn-round">#}
                        {#Get pricing#}
                        {#</a>#}

                    {#</div>#}

                {#</div>#}
                <div class="caption customin customout tp-resizeme posicion_logo"
                     data-x="right"
                     data-hoffset="0"
                     data-y="bottom"
                     data-voffset="-20"
                     data-hoffset="-220"
                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1800"
                     data-startslide="1"

                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-autoplay="false"
                     data-autoplayonlyfirsttime="false">

                    {#<iframe src="http://player.vimeo.com/video/56152991?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="480" height="275" allowfullscreen>#}
                    {#</iframe>#}
                    {{ image('images/general/logo_transparente.png','alt':'logo','class':'img-fluid','width':"200") }}

                </div>
            </li>
            <!-- End Slide Item -->


            <!-- Slide Item -->
            <li data-transition="fade" data-slotamount="7" data-masterspeed="1000" data-title="Bodas">

                <img src="images/home/003.jpg?Version1" alt="">


                {#<div class="caption customin customout tp-resizeme mediumlarge_light_white"#}
                     {#data-x="center"#}
                     {#data-y="center"#}
                     {#data-voffset="-70"#}
                     {#data-customin="x:-50;y:-300;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"#}
                     {#data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"#}

                     {#data-speed="800"#}
                     {#data-start="1000"#}
                     {#data-startslide="1"#}

                     {#data-easing="Power4.easeOut"#}
                     {#data-endspeed="500"#}
                     {#data-endeasing="Power4.easeIn">#}

                    {#<a href="http://vimeo.com/50201327" class="big-icon-link lightbox-gallery-1 mfp-iframe"><span class="big-icon big-icon-rs"><i class="fa fa-play-circle"></i></span></a>#}

                {#</div>#}

                <div class="slider_h1 caption customin customout tp-resizeme  "
                     data-x="left"
                     data-y="center"
                     data-hoffset="20"
                     data-voffset="40"
                     data-customin="x:-50;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.1;scaleY:0.1;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1300"
                     data-startslide="1"

                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn">


                    {#Rhythm Studio#}

                    <span class="slider_h1 titulo_slider padding_left_2em">  Bodas  </span>

                </div>


                {#<div class="caption customin customout tp-resizeme  no-transp "#}
                     {#data-x="center"#}
                     {#data-y="center"#}
                     {#data-voffset="120"#}
                     {#data-customin="x:50;y:300;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"#}
                     {#data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"#}
                     {#data-speed="800"#}
                     {#data-start="1500"#}
                     {#data-startslide="1"#}

                     {#data-easing="Power4.easeOut"#}
                     {#data-endspeed="500"#}
                     {#data-endeasing="Power4.easeIn">#}

                    {#Let&rsquo;s start work with&nbsp;us#}
                {#</div>#}
                <div class="caption customin customout tp-resizeme posicion_logo"
                     data-x="right"
                     data-hoffset="0"
                     data-y="bottom"
                     data-voffset="-20"
                     data-hoffset="-20"
                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1800"
                     data-startslide="1"

                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-autoplay="false"
                     data-autoplayonlyfirsttime="false">

                    {#<iframe src="http://player.vimeo.com/video/56152991?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="480" height="275" allowfullscreen>#}
                    {#</iframe>#}
                    {{ image('images/general/logo_transparente.png','alt':'logo','class':'img-fluid','width':"200") }}

                </div>

            </li>
            <!-- End Slide Item -->


            <!-- Slide Item -->
            <li data-transition="fade" data-slotamount="7" data-masterspeed="1000" data-title="Egresados">

                <img src="images/home/004.jpg?Version1" alt="">


                <div class="slider_h1 caption customin customout tp-resizeme  "
                     data-x="40"
                     data-y="bottom"
                     data-voffset="-80"
                     data-customin="x:-50;y:-300;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"

                     data-speed="800"
                     data-start="1000"
                     data-startslide="1"

                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn">

                    {#Introducing Video#}
                    <span class="slider_h1 titulo_slider ">  Egresados  </span>

                </div>

                {#<div class="caption fade customout tp-resizeme  "#}
                     {#data-x="30"#}
                     {#data-y="center"#}
                     {#data-voffset="-9"#}
                     {#data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"#}
                     {#data-speed="800"#}
                     {#data-start="1300"#}
                     {#data-startslide="1"#}

                     {#data-easing="Power4.easeOut"#}
                     {#data-endspeed="500"#}
                     {#data-endeasing="Power4.easeIn">#}

                    {#<span class="uppercase">We have art magic skills</span>#}

                {#</div>#}

                {#<div class="caption customin customout tp-resizeme large_bold_white"#}
                     {#data-x="30"#}
                     {#data-y="center"#}
                     {#data-voffset="75"#}
                     {#data-customin="x:-50;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.1;scaleY:0.1;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"#}
                     {#data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"#}
                     {#data-speed="800"#}
                     {#data-start="1500"#}
                     {#data-startslide="1"#}

                     {#data-easing="Power4.easeOut"#}
                     {#data-endspeed="500"#}
                     {#data-endeasing="Power4.easeIn">#}

                    {#<div class="local-scroll">#}

                        {#<a href="#about" class="btn btn-mod btn-border-w btn-medium btn-round">#}
                        {#Learn More#}
                        {#</a>#}

                    {#</div>#}

                {#</div>#}


                {#<div class="caption customin customout tp-resizeme"#}
                     {#data-x="right"#}
                     {#data-hoffset="0"#}
                     {#data-y="center"#}
                     {#data-voffset="0"#}
                     {#data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"#}
                     {#data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"#}
                     {#data-speed="800"#}
                     {#data-start="1800"#}
                     {#data-startslide="1"#}

                     {#data-easing="Power4.easeOut"#}
                     {#data-endspeed="500"#}
                     {#data-endeasing="Power4.easeIn"#}
                     {#data-autoplay="false"#}
                     {#data-autoplayonlyfirsttime="false">#}

                    {#<iframe src="http://player.vimeo.com/video/56152991?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="480" height="275" allowfullscreen>#}
                    {#</iframe>#}

                {#</div>#}
                <div class="caption customin customout tp-resizeme posicion_logo"
                     data-x="right"
                     data-hoffset="0"
                     data-y="bottom"
                     data-voffset="-20"
                     data-hoffset="-20"
                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1800"
                     data-startslide="1"

                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-autoplay="false"
                     data-autoplayonlyfirsttime="false">

                    {#<iframe src="http://player.vimeo.com/video/56152991?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="480" height="275" allowfullscreen>#}
                    {#</iframe>#}
                    {{ image('images/general/logo_transparente.png','alt':'logo','class':'img-fluid','width':"200") }}

                </div>

            </li>
            <!-- End Slide Item -->

            <!-- Slide Item -->
            <li data-transition="fade" data-slotamount="7" data-masterspeed="1000" data-title="Infantiles">

                <img src="images/home/005.jpg?Version1" alt="">


                <div class="caption customin customout tp-resizeme  "
                     data-x="30"
                     data-y="center"
                     data-voffset="-80"
                     data-customin="x:-50;y:-300;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"

                     data-speed="800"
                     data-start="1000"
                     data-startslide="1"

                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn">

                    {#Introducing Video#}

                </div>

                <div class="slider_h1 caption fade customout tp-resizeme  "
                     data-x="right"
                     data-y="center"
                     data-hoffset="-100"
                     data-voffset="20"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1300"
                     data-startslide="1"

                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn">

                    <span class="slider_h1 titulo_slider ">  Infantiles  </span>

                </div>

                {#<div class="caption customin customout tp-resizeme large_bold_white"#}
                     {#data-x="30"#}
                     {#data-y="center"#}
                     {#data-voffset="75"#}
                     {#data-customin="x:-50;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.1;scaleY:0.1;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"#}
                     {#data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"#}
                     {#data-speed="800"#}
                     {#data-start="1500"#}
                     {#data-startslide="1"#}

                     {#data-easing="Power4.easeOut"#}
                     {#data-endspeed="500"#}
                     {#data-endeasing="Power4.easeIn">#}

                    {#<div class="local-scroll">#}

                        {#<a href="#about" class="btn btn-mod btn-border-w btn-medium btn-round">#}
                        {#Learn More#}
                        {#</a>#}

                    {#</div>#}

                {#</div>#}


                {#<div class="caption customin customout tp-resizeme"#}
                     {#data-x="right"#}
                     {#data-hoffset="0"#}
                     {#data-y="bottom"#}
                     {#data-voffset="0"#}
                     {#data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"#}
                     {#data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"#}
                     {#data-speed="800"#}
                     {#data-start="1800"#}
                     {#data-startslide="1"#}

                     {#data-easing="Power4.easeOut"#}
                     {#data-endspeed="500"#}
                     {#data-endeasing="Power4.easeIn"#}
                     {#data-autoplay="false"#}
                     {#data-autoplayonlyfirsttime="false">#}

                    {#<iframe src="http://player.vimeo.com/video/56152991?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="480" height="275" allowfullscreen>#}
                    {#</iframe>#}

                {#</div>#}
                <div class="caption customin customout tp-resizeme posicion_logo"
                     data-x="right"
                     data-hoffset="0"
                     data-y="bottom"
                     data-voffset="-20"
                     data-hoffset="-20"
                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1800"
                     data-startslide="1"

                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-autoplay="false"
                     data-autoplayonlyfirsttime="false">

                    {#<iframe src="http://player.vimeo.com/video/56152991?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="480" height="275" allowfullscreen>#}
                    {#</iframe>#}
                    {{ image('images/general/logo_transparente.png','alt':'logo','class':'img-fluid','width':"200") }}

                </div>

            </li>
            <!-- End Slide Item -->
            <!-- EMPRESAS  Slide Item -->
            <li data-transition="fade" data-slotamount="7" data-masterspeed="1000" data-title="Comercio y Empresas">

                <img src="images/home/007.jpg?Version2" alt="">


                <div class="slider_h1 caption customin customout tp-resizeme  posicion_empresas " style=""
                     data-basealign="grid"
                     data-x="left"
                     data-y="top"

                     data-customin="x:-50;y:-300;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"

                     data-speed="800"
                     data-start="1000"
                     data-startslide="1"

                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn">

                    <span class="slider_h1 titulo_slider"> Despedí el año a tu medida!  </span>
                    {#Despedí el año a tu medida!#}

                </div>
                <div class=" caption customin customout tp-resizeme  posicion_empresas_sub "
                     data-x="right"
                     data-y="top"
                     data-voffset="200"
                     data-hoffset="-100"

                     data-customin="x:-50;y:-300;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"

                     data-speed="800"
                     data-start="1000"
                     data-startslide="1"

                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn">

                    <span class="slider_h1 mini_titulo "> Comercio y Empresas  </span>


                </div>

                {#<div class="slider_h1 caption fade customout tp-resizeme   "#}
                {#data-x="left"#}
                {#data-y="top"#}
                {#data-voffset="200"#}
                {#data-hoffset = "20"#}
                {#data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"#}
                {#data-speed="800"#}
                {#data-start="1300"#}
                {#data-startslide="1"#}

                {#data-easing="Power4.easeOut"#}
                {#data-endspeed="500"#}
                {#data-endeasing="Power4.easeIn">#}

                {#<span class="slider_h1 subtitulo_slider" >#}
                {#Comercio y Empresas </span>#}

                {#</div>#}

                {#<div class="caption customin customout tp-resizeme large_bold_white"#}
                     {#data-x="30"#}
                     {#data-y="bottom"#}
                     {#data-voffset="75"#}
                     {#data-customin="x:-50;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.1;scaleY:0.1;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"#}
                     {#data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"#}
                     {#data-speed="800"#}
                     {#data-start="1500"#}
                     {#data-startslide="1"#}

                     {#data-easing="Power4.easeOut"#}
                     {#data-endspeed="500"#}
                     {#data-endeasing="Power4.easeIn">#}

                    {#<div class="local-scroll">#}

                        {#{{ image('images/general/logo_transparente.png','alt':'logo','class':'img-fluid','width':"200") }}#}


                    {#</div>#}

                {#</div>#}


                <div class="caption customin customout tp-resizeme posicion_logo"
                     data-x="right"
                     data-hoffset="0"
                     data-y="bottom"
                     data-voffset="-20"
                     data-hoffset="-20"
                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1800"
                     data-startslide="1"

                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-autoplay="false"
                     data-autoplayonlyfirsttime="false">

                    {#<iframe src="http://player.vimeo.com/video/56152991?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="480" height="275" allowfullscreen>#}
                    {#</iframe>#}
                    {{ image('images/general/logo_transparente.png','alt':'logo','class':'img-fluid','width':"200") }}

                </div>


            </li>
            <!-- End Slide Item -->

        </ul>
        {# Banner timer #}
        <div class="tp-bannertimer tp-bottom"></div>
    </div>
</div>
<!-- End Home Section -->