{#<!-- Portfolio Section -->#}
{#-------------------------------------------#}
<section id="servicios" class="page-section bg-dark">
    <div class="pattern"></div>
    <div class="container relative">
        <h2 class="section-title font-alt mb-70 mb-sm-40">
            Servicios
        </h2>

        <p>
            Nuestros eventos reflejan la responsabilidad y dedicación con la que escuchamos y cumplimos los deseos de
            cada cliente. Te proponemos que dejes todo en nuestras manos, y que te prepares solo para disfrutar de un
            evento único e inolvidable, sin estrés ni preocupaciones.
        </p>

        <!-- Works Grid -->
        <ul class="works-grid work-grid-3 work-grid-gut clearfix font-alt hover-white" id="work-grid">

            <!-- Work Item (Lightbox) -->
            <li class="work-item mix photography">
                <a href="#" data-featherlight="#catering" data-featherlight-variant="fixwidth">
                    <div class="work-img">
                        {{ image('images/servicios/01.jpg', 'alt':'servicio de catering') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">Catering</h3>

                        <div class="work-descr">
                            Un universo de sensaciones
                        </div>
                    </div>
                </a>
            </li>
            <!-- End Work Item -->

            <!-- Work Item (External Page) -->
            <li class="work-item mix branding design">
                <a href="#" data-featherlight="#ambientacion" data-featherlight-variant="fixwidth">

                    <div class="work-img">
                        {{ image('images/servicios/02.jpg', 'alt':'servicio de ambientacion') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">Ambientación</h3>

                        <div class="work-descr">
                            Servicio de Decoración y Ambientación de eventos
                        </div>
                    </div>
                </a>
            </li>
            <!-- End Work Item -->

            <!-- Work Item (External Page) -->
            <li class="work-item mix branding">
                <a href="#" data-featherlight="#barra_tragos" data-featherlight-variant="fixwidth">

                    <div class="work-img">
                        {{ image('images/servicios/03.jpg', 'alt':'servicio de tragos') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">Barra de Tragos</h3>

                        <div class="work-descr">
                            Barra libre y sin cortes
                        </div>
                    </div>
                </a>
            </li>
            <!-- End Work Item -->

            <!-- Work Item (External Page) -->
            <li class="work-item mix design photography">
                <a href="#" data-featherlight="#sonido_iluminacion" data-featherlight-variant="fixwidth">
                    <div class="work-img">
                        {{ image('images/servicios/04.jpg', 'alt':'servicio de sonido') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">Sonido e iluminación</h3>

                        <div class="work-descr">
                            El más completo servicio para tu diversión
                        </div>
                    </div>
                </a>
            </li>
            <!-- End Work Item -->

            <!-- Work Item (External Page) -->
            <li class="work-item mix design">
                <a href="#" data-featherlight="#mozos" data-featherlight-variant="fixwidth">
                    <div class="work-img">
                        {{ image('images/servicios/05.jpg', 'alt':'servicio de mozos') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">Mozos</h3>

                        <div class="work-descr">
                            Todo lo que tus invitados necesitan para sentirse bien atendidos!
                        </div>
                    </div>
                </a>
            </li>
            <!-- End Work Item -->

            <!-- Work Item (External Page) -->
            <li class="work-item mix branding design">
                <a href="#" data-featherlight="#vajilla_manteleria" data-featherlight-variant="fixwidth">
                    <div class="work-img">
                        {{ image('images/servicios/06.jpg', 'alt':'servicio de vajilla') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">Vajilla y Mantelería</h3>

                        <div class="work-descr">
                            La decoración ideal para cada evento
                        </div>
                    </div>
                </a>
            </li>
            <!-- End Work Item -->

            <!-- Work Item (External Page) -->
            <li class="work-item mix branding">
                <a href="#" data-featherlight="#fotografia" data-featherlight-variant="fixwidth">
                    <div class="work-img">
                        {{ image('images/servicios/07.jpg?1', 'alt':'servicio de fotografia') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">Fotografía</h3>

                        <div class="work-descr">
                            Que los mejores momentos de tu evento queden inmortalizados.
                        </div>
                    </div>
                </a>
            </li>
            <!-- End Work Item -->

            <!-- Work Item (External Page) -->
            <li class="work-item mix design photography">
                <a href="#" data-featherlight="#invitaciones" data-featherlight-variant="fixwidth">
                    <div class="work-img">
                        {{ image('images/servicios/08.jpg', 'alt':'servicio de invitaciones') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">Invitaciones</h3>

                        <div class="work-descr">
                            Tus invitados estarán interesados en tu evento desde el primer momento.
                        </div>
                    </div>
                </a>
            </li>
            <!-- End Work Item -->

            <!-- Work Item (External Page) -->
            <li class="work-item mix branding design">
                <a href="#" data-featherlight="#mesa_dulce_torta" data-featherlight-variant="fixwidth">
                    <div class="work-img">
                        {{ image('images/servicios/09.jpg', 'alt':'servicio de mesa dulce') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">Mesa dulce y torta</h3>

                        <div class="work-descr">
                            Agasaja a tus invitados con toda la dulzura de nuestras mesas dulces.
                        </div>
                    </div>
                </a>
            </li>
            <!-- End Work Item -->
        </ul>
        <!-- End Works Grid -->

    </div>
</section>
<!-- End Portfolio Section -->
