<!-- Testimonials Section -->
<section class="page-section  fullwidth-slider" data-background="images/clientes/01.jpg?3">

    <!-- Slide Item -->
    <div>
        <div class="container relative">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 align-center testimonio_bg">
                    <!-- Section Icon -->
                    <div class="section-icon">
                        <span class="icon-quote"></span>
                    </div>
                    <h3 class="  small-title font-alt">Que dicen nuestros clientes?</h3>
                    <blockquote class="testimonial white">
                        <p>
                            No me canso ni me voy a cansar de decir..!! Gracias Facu, Gabi y a todo su equipo.. hicieron nuestro sueño realidad..!
                        </p>
                        <footer class="testimonial-author">
                            Diego Nicolas Castillo
                        </footer>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
    <!-- End Slide Item -->
    <!-- Slide Item -->
    <div>
        <div class="container relative">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 align-center testimonio_bg">
                    <!-- Section Icon -->
                    <div class="section-icon">
                        <span class="icon-quote"></span>
                    </div>
                    <h3 class="  small-title font-alt">Que dicen nuestros clientes?</h3>
                    <blockquote class="testimonial white">
                        <p>
                            Capitán Cook! Excelente servicio que nos brindó para el cumple de 15 de Huilen!!!
                            Todo lo que soñamos para esa noche salió de 10!!!!
                        </p>
                        <footer class="testimonial-author">
                           Paola Necul
                        </footer>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
    <!-- End Slide Item -->

    <!-- Slide Item -->
    <div>
        <div class="container relative">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 align-center testimonio_bg">
                    <!-- Section Icon -->
                    <div class="section-icon">
                        <span class="icon-quote"></span>
                    </div>
                    <h3 class="  small-title font-alt">Que dicen nuestros clientes?</h3>
                    <blockquote class="testimonial white">
                        <p>
                            Sin palabras!!! La verdad que no sufrí ningún tipo de estrés... son un equipo genial, estan en cada detalle... Gracias gracias a todos hicieron un gran trabajo... Nos volveremos a ver en los 15 de Maia y 18 de Lucas!!
                        </p>
                        <footer class="testimonial-author">
                            Mony Herrera
                        </footer>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
    <!-- End Slide Item -->
    <!-- Slide Item -->
    <div>
        <div class="container relative">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 align-center testimonio_bg">
                    <!-- Section Icon -->
                    <div class="section-icon">
                        <span class="icon-quote "></span>
                    </div>
                    <!-- Section Title -->
                    <h3 class=" small-title font-alt">Que dicen nuestros clientes?</h3>
                    <blockquote class="testimonial white">
                        <p>
                            La verdad muy linda y buena organizacion. Te sentas a disfrutar la fiesta ya que ellos se encargan de todo. La fiesta de egresados de septimo del colegio Ecen fue realizada por ellosy la verdad que estan en todos los detalles. Los papas quedamos muy contentos y los chicos disfrutaron mucho. Mil gracias Capitan Cook                        </p>
                        <footer class="testimonial-author">
                            Silvina Rivera
                        </footer>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
    <!-- End Slide Item -->


</section>
<!-- End Testimonials Section -->
<!-- Logotypes Section -->
{#<section class="small-section pt-20 pb-20 bg-dark">#}
    {#<div class="pattern"></div>#}
    {#<div class="container relative">#}

        {#<div class="row">#}
            {#<div class="col-md-10 col-md-offset-1">#}

                {#<div class="small-item-carousel black owl-carousel mb-0 animate-init" data-anim-type="fade-in-right-large" data-anim-delay="100">#}

                    {#<!-- Logo Item -->#}
                    {#<div class="logo-item">#}
                        {#<img src="images/clients-logos/client-1.png" width="67" height="67" alt="" />#}
                    {#</div>#}
                    {#<!-- End Logo Item -->#}

                    {#<!-- Logo Item -->#}
                    {#<div class="logo-item">#}
                        {#<img src="images/clients-logos/client-2.png" width="67" height="67" alt="" />#}
                    {#</div>#}
                    {#<!-- End Logo Item -->#}

                    {#<!-- Logo Item -->#}
                    {#<div class="logo-item">#}
                        {#<img src="images/clients-logos/client-3.png" width="67" height="67" alt="" />#}
                    {#</div>#}
                    {#<!-- End Logo Item -->#}

                    {#<!-- Logo Item -->#}
                    {#<div class="logo-item">#}
                        {#<img src="images/clients-logos/client-4.png" width="67" height="67" alt="" />#}
                    {#</div>#}
                    {#<!-- End Logo Item -->#}

                    {#<!-- Logo Item -->#}
                    {#<div class="logo-item">#}
                        {#<img src="images/clients-logos/client-5.png" width="67" height="67" alt="" />#}
                    {#</div>#}
                    {#<!-- End Logo Item -->#}

                    {#<!-- Logo Item -->#}
                    {#<div class="logo-item">#}
                        {#<img src="images/clients-logos/client-6.png" width="67" height="67" alt="" />#}
                    {#</div>#}
                    {#<!-- End Logo Item -->#}

                    {#<!-- Logo Item -->#}
                    {#<div class="logo-item">#}
                        {#<img src="images/clients-logos/client-1.png" width="67" height="67" alt="" />#}
                    {#</div>#}
                    {#<!-- End Logo Item -->#}

                    {#<!-- Logo Item -->#}
                    {#<div class="logo-item">#}
                        {#<img src="images/clients-logos/client-2.png" width="67" height="67" alt="" />#}
                    {#</div>#}
                    {#<!-- End Logo Item -->#}

                {#</div>#}

            {#</div>#}
        {#</div>#}

    {#</div>#}
{#</section>#}
<!-- End Logotypes -->
