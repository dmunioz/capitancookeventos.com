<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <title>[SUBJECT]</title>
    <style type="text/css">
        body {
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            margin: 0 !important;
            width: 100% !important;
            -webkit-text-size-adjust: 100% !important;
            -ms-text-size-adjust: 100% !important;
            -webkit-font-smoothing: antialiased !important;
        }

        .tableContent img {
            border: 0 !important;
            display: block !important;
            outline: none !important;
        }

        ul {
            margin: 0;
        }

        p {
            color: #999999;
            font-size: 13px;
            line-height: 19px;
            margin: 0;
        }

        h2, h1 {
            color: #555555;
            font-size: 21px;
            font-weight: normal;
            margin: 0;
        }

        h2.white {
            color: #ffffff;
        }

        a {
            color: #000000;
        }

        a.link1 {
            font-size: 13px;
            color: #77529E;
            font-weight: bold;
            text-decoration: none;
        }

        a.link2 {
            padding: 8px;
            background: #77529E;
            font-size: 13px;
            color: #ffffff;
            text-decoration: none;
            font-weight: bold;
        }

        .bgBody {
            background: #F6F6F6;
        }

        .bgItem {
            background: #ffffff;
        }

        @media only screen and (max-width: 480px) {

            table[class="MainContainer"], td[class="cell"] {
                width: 100% !important;
                height: auto !important;
            }

            td[class="specbundle"] {
                width: 100% !important;
                float: left !important;
                font-size: 14px !important;
                line-height: 18px !important;
                display: block !important;
                padding-bottom: 15px !important;
            }

            td[class="spechide"] {
                display: none !important;
            }

            img[class="banner"] {
                width: 100% !important;
                height: auto !important;
            }

        }

        @media only screen and (max-width: 540px) {

            table[class="MainContainer"], td[class="cell"] {
                width: 100% !important;
                height: auto !important;
            }

            td[class="specbundle"] {
                width: 100% !important;
                float: left !important;
                font-size: 14px !important;
                line-height: 18px !important;
                display: block !important;
                padding-bottom: 15px !important;
            }

            td[class="spechide"] {
                display: none !important;
            }

            img[class="banner"] {
                width: 100% !important;
                height: auto !important;
            }

        }


    </style>

    <script type="colorScheme" class="swatch active">
  {
    "name":"Default",
    "bgBody":"F6F6F6",
    "link":"77529E",
    "color":"999999",
    "bgItem":"ffffff",
    "title":"555555"
  }



    </script>
</head>
<body class='bgBody' leftpadding="0" offset="0" paddingheight="0" paddingwidth="0"
      style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;"
      toppadding="0">
<table align="center" bgcolor="#f6f6f6" border="0" cellpadding="0" cellspacing="0" class="tableContent" width="100%">
    <tbody>
    <tr>
        <td>
            <table align="center" bgcolor="#f6f6f6" border="0" cellspacing="0"
                   style="font-family:helvetica, sans-serif;" width="600" class="MainContainer">
                <tbody>
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td valign="top" width="20">&nbsp;</td>
                                <td valign="top">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                        <!--  =========================== The header ===========================  -->
                                        <tbody>
                                        <tr>
                                            <!--  =========================== The body ===========================  -->
                                            <td class="movableContentContainer">
                                                <div class="movableContent"
                                                     style="border: 0px; padding-top: 0px; position: relative;">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td height="25">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table align="center" border="0" cellpadding="0"
                                                                       cellspacing="0" width="100%">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td align="left" valign="middle">
                                                                            <div class="contentEditableContainer contentImageEditable">
                                                                                <div class="contentEditable">
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td align="right" valign="top">
                                                                            <div class="contentEditableContainer contentImageEditable"
                                                                                 style="display:inline-block;">
                                                                                <div class="contentEditable">
                                                                                    Por
                                                                                    favor NO responda este
                                                                                    e-mail.</div>
                                                                            </div>
                                                                        </td>
                                                                        <td align="right" valign="top" width="18">
                                                                            <div class="contentEditableContainer contentImageEditable"
                                                                                 style="display:inline-block;">
                                                                                <div class="contentEditable">
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="movableContent"
                                                     style="border: 0px; padding-top: 0px; position: relative;">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td height="25">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" bgcolor="#52619E" height="184"
                                                                style="background-size:560px 184px;" valign="middle"
                                                                width="560">
                                                                <div class="contentEditableContainer contentImageEditable">
                                                                    <div class="contentEditable">
                                                                        <img class="banner" alt="product image"
                                                                             data-default="placeholder"
                                                                             data-max-width="560" height="184"
                                                                             src="cid:email_header" width="560"/>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="movableContent"
                                                     style="border: 0px; padding-top: 0px; position: relative;">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="right" bgcolor="#3e3852"
                                                                style="border-top: 2px solid white;padding:20px;">
                                                                <div class="contentEditableContainer contentTextEditable">
                                                                    <div class="contentEditable">
                                                                        <p style="color:#ffffff;font-size:16px;line-height:19px;">
                                                                            IMPS - CONSULTA WEB</p>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="movableContent"
                                                     style="border: 0px; padding-top: 0px; position: relative;">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td height="25">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table align="center" class='bgItem' border="0"
                                                                       cellpadding="0" cellspacing="0"
                                                                       style="border:1px solid #dddddd" valign="top"
                                                                       width="100%">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td colspan="3" height="16">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="16">
                                                                        </td>
                                                                        <td>
                                                                            <table align="center" class='bgItem'
                                                                                   border="0" cellpadding="0"
                                                                                   cellspacing="0" valign="top"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td align="left" valign="top">
                                                                                        <div class="contentEditableContainer contentTextEditable">
                                                                                            <div class="contentEditable">
                                                                                                <h2> MENSAJE ENVIADO DESDE IMPS.ORG.AR</h2>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="13">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" valign="top">
                                                                                        <div class="contentEditableContainer contentTextEditable">
                                                                                            <div class="contentEditable">
                                                                                                {#<p> <b>NOMBRE COMPLETO: </b> {{ nombre }} </p>#}
                                                                                                {#<p> <b>EMAIL: </b> {{ email }} </p>#}
                                                                                                {#<p> <b>ASUNTO: </b> {{ asunto }} </p>#}
                                                                                                {#<p> <b>MENSAJE: </b> {{ mensaje }}#}
                                                                                                {#</p>#}
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="20">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right"
                                                                                        style="padding-bottom:8px;"
                                                                                        valign="top">
                                                                                        <div class="contentEditableContainer contentTextEditable">
                                                                                            <div class="contentEditable">

                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td width="16">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" height="16">
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>


                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td valign="top" width="20">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
