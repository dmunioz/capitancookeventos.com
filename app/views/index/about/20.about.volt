<!-- About Section -->
<section class="page-section">
    <div class="relative">

        <div class="container">

            <h2 class="section-title font-alt mb-70 mb-sm-40">
                Two Freelance Photographers
            </h2>

            <div class="row mb-100 mb-sm-70">
                <div class="col-md-8 col-md-offset-2">

                    <div class="section-text align-center mb-70 mb-xs-40">
                        In&nbsp;auctor ex&nbsp;id&nbsp;urna faucibus porttitor. Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit. In&nbsp;maximus ligula semper metus pellentesque mattis.
                        Maecenas volutpat, diam enim sagittis quam, id&nbsp;porta quam. Sed id&nbsp;dolor
                        consectetur fermentum nibh volutpat, accumsan purus.
                    </div>

                    <div class="align-center">
                        <img src="images/photographer/signature.png" width="170" height="55" alt="" />
                    </div>

                </div>
            </div>

            <div class="row">

                <!-- Team item -->
                <div class="col-sm-4 col-sm-offset-2 mb-xs-30 wow fadeInUp">
                    <div class="team-item">

                        <div class="team-item-image">

                            <img src="images/team/team-1.jpg" alt="" />

                            <div class="team-item-detail">

                                <h4 class="font-alt normal">Hello & Welcome!</h4>

                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit lacus, a&nbsp;iaculis diam.
                                </p>

                                <div class="team-social-links">
                                    <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                    <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                    <a href="#" target="_blank"><i class="fa fa-pinterest"></i></a>
                                </div>

                            </div>
                        </div>

                        <div class="team-item-descr font-alt">

                            <div class="team-item-name">
                                Thomas Rhythm
                            </div>

                            <div class="team-item-role">
                                Art Director
                            </div>

                        </div>

                    </div>
                </div>
                <!-- End Team item -->

                <!-- Team item -->
                <div class="col-sm-4 mb-xs-30 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="team-item">

                        <div class="team-item-image">

                            <img src="images/team/team-2.jpg" alt="" />

                            <div class="team-item-detail">

                                <h4 class="font-alt normal">Nice to meet!</h4>

                                <p>
                                    Curabitur augue, nec finibus mauris pretium eu. Duis placerat ex gravida nibh tristique porta.
                                </p>

                                <div class="team-social-links">
                                    <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                    <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                    <a href="#" target="_blank"><i class="fa fa-pinterest"></i></a>
                                </div>

                            </div>
                        </div>

                        <div class="team-item-descr font-alt">

                            <div class="team-item-name">
                                Marta Laning
                            </div>

                            <div class="team-item-role">
                                Web engineer
                            </div>

                        </div>

                    </div>
                </div>
                <!-- End Team item -->

            </div>

        </div>

    </div>
</section>
<!-- End About Section -->