<!-- About Section -->
<section class="page-section bg-gray-lighter">
    <div class="relative">

        <div class="container">

            <h2 class="section-title font-alt mb-70 mb-sm-40">
                Capitán Cook
            </h2>

            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <div class="section-text align-center mb-70 mb-xs-40">
                        Planificamos, organizamos y coordinamos las fiestas más originales, completas y divertidas.

                        Tenemos las mejores y más variadas alternativas de servicios. Vivimos junto a vos el evento
                        supervisando todos los detalles para que todo sea perfecto.

                        Nuestros eventos reflejan la responsabilidad y dedicación con la que escuchamos y cumplimos los
                        deseos de cada cliente, dando siempre lo mejor de nosotros para que nada falle y tu evento sea
                        original, lleno de detalles exquisitos.

                        Te proponemos que dejes todo en nuestras manos, y que te prepares solo para disfrutar de un
                        evento único e inolvidable, sin estrés ni preocupaciones.
                    </div>

                    <div class="align-center">
                        <img src="images/photographer/signature.png" width="170" height="55" alt=""/>
                    </div>

                </div>
            </div>

        </div>

    </div>
</section>
<!-- End About Section -->