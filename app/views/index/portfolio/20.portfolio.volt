<!-- Portfolio Section -->
<style>
    .work-item:hover .work-img:after {
        background: rgba(0, 170, 0, 0.22) !important;
    }
    .lazy{
        height: 400px;
        max-height: 400px;
    }

    @media only screen and (max-width: 1200px) {
        .lazy{
           height: 400px !important;
            max-height: 400px;
        }
    }

    @media only screen and (max-width: 767px) {
        .lazy{
            height: 300px !important;;
            max-height: 300px;
        }
    }

    @media only screen and (max-width: 480px) {
        .lazy{
            height: 200px !important;;
            max-height: 200px;
        }
    }
</style>
<section class="page-section pb-0 bg-dark-alfa-90">
    <div class="pattern_2">
    </div>
    <div class="relative">
        {#<img data-src="https://unsplash.it/600/350?image=54" src="http://placehold.it/600x350&text=Placeholder" alt="">#}
        {#<img data-src="https://unsplash.it/600/350?image=52" src="http://placehold.it/600x350&text=Placeholder" alt="">#}
        {#<img data-src="https://unsplash.it/600/350?image=51" src="http://placehold.it/600x350&text=Placeholder" alt="">#}
        {#<img data-src="https://unsplash.it/600/350?image=55" src="http://placehold.it/600x350&text=Placeholder" alt="">#}
        {#<img data-src="https://unsplash.it/600/350?image=56" src="http://placehold.it/600x350&text=Placeholder" alt="">#}
        {#<img data-src="https://unsplash.it/600/350?image=57" src="http://placehold.it/600x350&text=Placeholder" alt="">#}
        {#<img data-src="https://unsplash.it/600/350?image=58" src="http://placehold.it/600x350&text=Placeholder" alt="">#}
        {#<img data-src="https://unsplash.it/600/350?image=59" src="http://placehold.it/600x350&text=Placeholder" alt="">#}
        {#<img data-src="https://unsplash.it/600/350?image=60" src="http://placehold.it/600x350&text=Placeholder" alt="">#}
        {#<!-- Works Filter -->#}
        {#<div class="works-filter font-alt align-center">#}
        {#<a href="#" class="filter active" data-filter="*">Todas las fotos</a>#}
        {#<a href="#egresados" class="filter" data-filter=".egresados">Egresados</a>#}
        {#<a href="#cumpleaños" class="filter" data-filter=".cumpleaños">Cumpleaños</a>#}
        {#<a href="#bodas" class="filter" data-filter=".bodas">Casamiento</a>#}
        {#<a href="#empresas" class="filter" data-filter=".empresas">Empresas</a>#}
        {#</div>#}
        {#<!-- End Works Filter -->#}

        <!-- Works Grid -->
        <ul  class="works-grid work-grid-3  font-alt hover-white hide-titles" id="work-grid">
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/1.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/1.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/1.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/3.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/3.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/3.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/4.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/4.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/4.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/5.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/5.jpg?1', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/5.jpg?1') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/6.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/6.jpg?1', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/6.jpg?1') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/7.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/7.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/7.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/8.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/8.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/8.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/9.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/9.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/9.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/10.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/10.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/10.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/11.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/11.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/11.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/12.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/12.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/12.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/13.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/13.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/13.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/14.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/14.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/14.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="images/galeria_completa/15.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/15.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/15.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/16.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/16.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/16.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/17.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/17.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/17.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/18.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/18.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/18.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/19.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/19.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/19.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/20.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/20.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/20.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/21.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/21.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/21.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/22.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/22.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/22.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/23.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/23.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/23.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/24.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/24.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/24.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/25.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/25.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/25.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/26.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/26.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/26.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/27.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/27.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/27.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/28.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/28.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/28.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/29.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/29.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/29.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/30.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/30.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/30.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/31.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/31.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/31.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/32.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/32.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/32.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/33.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/33.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/33.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/34.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/34.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/34.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/35.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/35.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/35.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/36.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/36.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/36.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/37.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/37.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/37.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/38.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/38.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/38.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/39.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/39.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/39.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/40.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/40.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/40.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/41.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/41.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/41.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/42.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/42.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/42.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/43.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/43.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/43.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/44.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/44.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/44.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/45.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/45.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/45.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/46.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/46.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/46.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/47.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/47.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/47.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/48.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/48.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/48.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/49.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/49.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/49.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/50.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/50.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/50.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/51.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/51.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/51.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/52.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/52.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/52.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/53.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/53.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/53.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/54.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/54.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/54.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/55.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/55.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/55.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/56.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/56.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/56.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/57.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/57.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/57.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/58.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/58.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/58.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/59.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/59.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/59.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/60.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/60.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/60.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/61.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/61.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/61.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/62.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/62.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/62.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/63.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/63.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/63.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/64.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/64.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/64.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/65.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/65.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/65.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/66.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/66.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/66.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/67.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/67.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/67.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/68.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/68.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/68.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/69.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/69.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/69.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/70.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/70.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/70.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/71.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/71.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/71.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/72.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/72.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/72.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/73.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/73.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/73.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/74.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/74.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/74.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/75.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/75.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/75.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/76.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/76.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/76.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/77.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/77.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/77.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/78.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/78.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/78.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/79.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/79.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/79.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/80.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/80.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/80.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/81.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/81.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/81.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/82.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/82.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/82.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/83.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/83.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/83.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/84.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/84.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/84.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/85.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/85.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/85.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/86.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/86.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/86.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/87.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/87.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/87.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            <!-- Work Item (Lightbox) -->
            <li class="work-item">
                <a href="/images/galeria_completa/88.jpg"
                   class="work-lightbox-link mfp-image">
                    <div class="work-img">
                        {{ image('images/galeria_completa/88.jpg', 'alt':'galeria', 'class':'lazy img-fluid',
                        'data-src':'/images/galeria_completa/88.jpg') }}
                    </div>
                    <div class="work-intro">
                        <h3 class="work-title">CapitanCookEventos.com</h3>

                        <div class="work-descr">

                        </div>
                    </div>
                </a>
            </li>
            {#{% for imagen in galeria %}#}
                {#<li class="work-item">#}
                    {#<a href="/images/galeria_completa/{{ imagen['nombre']  }}"#}
                       {#class="work-lightbox-link mfp-image">#}
                        {#<div class="work-img">#}
                            {#{{ image('images/galeria_completa/'~imagen['nombre'] , 'alt':'galeria', 'class':'lazy img-fluid',#}
                             {#'data-src':'/images/galeria_completa/'~imagen['nombre'] ) }}#}
                        {#</div>#}
                        {#<div class="work-intro">#}
                            {#<h3 class="work-title">CapitanCookEventos.com</h3>#}

                            {#<div class="work-descr">#}

                            {#</div>#}
                        {#</div>#}
                    {#</a>#}
                {#</li>#}

            {#{% endfor %}#}


        </ul>
        <!-- End Works Grid -->

    </div>
</section>
<!-- End Portfolio Section -->
            