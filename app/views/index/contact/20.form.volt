<!-- Section -->
<section class="page-section">
    <div class="relative">

        <div class="container">

            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <div class="section-text align-center mb-70 mb-xs-40">
                       Presupuesto sin cargo
                    </div>

                    <div class="row">

                        <div class="col-sm-5 mb-sm-60">

                            <!-- Phone -->
                            <div class="contact-item mb-30">
                                <div class="ci-icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="ci-title font-alt">
                                    Call Us
                                </div>
                                <div class="ci-text">
                                    299 6560957
                                </div>
                            </div>
                            <!-- End Phone -->

                            <hr class="mb-30"/>

                            <!-- Address -->
                            <div class="contact-item mb-30">
                                <div class="ci-icon">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <div class="ci-title font-alt">
                                    Dirección
                                </div>
                                <div class="ci-text">
                                    Nos encontramos a un llamado de distancia
                                </div>
                            </div>
                            <!-- End Address -->

                            <hr class="mb-30"/>

                            <!-- Email -->
                            <div class="contact-item">
                                <div class="ci-icon">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <div class="ci-title font-alt">
                                    Email
                                </div>
                                <div class="ci-text">
                                    <a href="mailto:support@bestlooker.pro">capitancookeventos@gmail.com</a>
                                </div>
                            </div>
                            <!-- End Email -->

                        </div>

                        <div class="col-sm-7">

                            <form class="form contact-form" id="contact_form">
                                <div class="clearfix">

                                    <!-- Name -->
                                    <div class="form-group">
                                        <input type="text" name="name" id="name" class="input-md round form-control" placeholder="Nombre" pattern=".{3,100}" required>
                                    </div>

                                    <!-- Email -->
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" class="input-md round form-control" placeholder="Email" pattern=".{5,100}" required>
                                    </div>

                                    <!-- Message -->
                                    <div class="form-group">
                                        <textarea name="message" id="message" class="input-md round form-control" style="height: 100px;" placeholder="Mensaje"></textarea>
                                    </div>

                                </div>

                                <div class="clearfix">

                                    <div class="cf-left-col">

                                        <!-- Inform Tip -->
                                        <div class="form-tip pt-20">
                                            <i class="fa fa-info-circle"></i> Todos los campos son requeridos
                                        </div>

                                    </div>

                                    <div class="cf-right-col">

                                        <!-- Send Button -->
                                        <div class="align-right pt-10">
                                            <button class="submit_btn btn btn-mod btn-medium btn-round" id="submit_btn">Submit</button>
                                        </div>

                                    </div>

                                </div>

                                <div id="result"></div>
                            </form>

                        </div>

                    </div>

                </div>
            </div>


        </div>

    </div>
</section>
<!-- End Section -->