<!-- Top Bar -->

<div class="top-bar">
    <div id="barra_contacto" class="container clearfix">

        <!-- Top Links -->
        <ul class="top-links left tooltip-bot">
            <li class="hidden-sm hidden-xs">
                <a href="https://api.whatsapp.com/send?phone=5492996560957"
                   target="_blank"
                   title="Whatsapp"  >
                    <i class="fa fa-whatsapp" aria-hidden="true"></i>+54 9 299 656-0957
                </a>
            </li>
            <li class="hidden-sm hidden-xs">
                <a href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=capitancookeventos@gmail.com"
                   title="Correo" target="_blank" class="white text-lowercase">capitancookeventos@gmail.com</a>
            </li>
        </ul>
        <!-- End Top Links -->

        <!-- Social Links -->
        <ul class="top-links right tooltip-bot">
            <li class="hidden-lg hidden-md">
                <a href="https://api.whatsapp.com/send?phone=5492996560957"
                   target="_blank"
                   title="Whatsapp"  >
                    <i class="fa fa-whatsapp" aria-hidden="true"></i>
                </a>
            </li>
            <li class="hidden-lg hidden-md">
                <a href="mailto:capitancookeventos@gmail.com" title="Correo">
                    <i class="fa fa-envelope"></i> </a>
            </li>
            <li><a href="https://m.me/785330554890487?fbclid=IwAR2eBZkMWOVi0amDd0QeUAL6QFnumI4v3i88761DOIkO6ZvW7B-j5Sh7FCI" title="Facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://open.spotify.com/user/capit%C3%A1ncook?si=G081uupZSqGeS4pyRDJciQ" title="Spotify"><i class="fa fa-spotify"></i></a></li>
            <li><a href="https://www.instagram.com/capitancook.nqn/" title="Instagram"><i class="fa fa-instagram"></i></a></li>
        </ul>
        <!-- End Social Links -->

    </div>
</div>
<!-- End Top Bar -->

<!-- Navigation panel -->
{#main-nav dark stick-fixed js-transparent transparent#}
<nav class="main-nav js-stick dark">
    <div class="container relative clearfix">
        <!-- Logo ( * your text or image into link tag *) -->
        <div class="nav-logo-wrap local-scroll">
            {{ link_to('','class':'logo', image('images/general/capitan.png','alt':'logo')) }}
        </div>
        <div class="mobile-nav">
            <i class="fa fa-bars"></i>
        </div>

        <!-- Main Menu -->
        <div class="inner-nav desktop-nav">
            <ul class="clearlist scroll-nav local-scroll">
                <li class="active">
                    {{ link_to('#home','Home') }}
                </li>
                <li>
                    {{ link_to('/#nosotros','Nosotros') }}
                </li>
                <li>
                    {{ link_to('/#servicios','Servicios') }}
                </li>
                <li>
                    {{ link_to('/#galeria','Galeria') }}
                </li>
                <li>
                    {{ link_to('/#blog','Blog') }}
                </li>
                <li>
                    {{ link_to('/#contactos','Contactos') }}
                </li>
            </ul>
        </div>
        <!-- End Main Menu -->

    </div>
</nav>
<!-- End Navigation panel -->

{{ content() }}