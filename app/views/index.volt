<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Capitán Cook</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="utf-8">
    <meta name="author" content="Muñoz Daniel Eduardo">
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- Favicons -->
    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
    <script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
    <!-- CSS -->
    {{ stylesheet_link('css/bootstrap.min.css') }}
    {{ stylesheet_link('css/style.css?32') }}
    {{ stylesheet_link('css/style-responsive.css?17') }}
    {{ stylesheet_link('css/animate.min.css') }}
    {{ stylesheet_link('css/vertical-rhythm.min.css') }}
    {{ stylesheet_link('css/owl.carousel.css') }}
    {{ stylesheet_link('css/magnific-popup.css') }}

    {{ stylesheet_link('css/rev-slider.css') }}
    {{ stylesheet_link('rs-plugin/css/settings.css') }}
    {{ stylesheet_link('css/featherlight.css') }}
    {{ stylesheet_link('css/justifiedGallery.min.css') }}


    {# CSS y JS individuales #}
    {% if (assets.collection("headerJQuery")) %}
        {{ assets.outputJs("headerJQuery") }}
    {% endif %}
    {% if (assets.collection("headerCss")) %}
        {{ assets.outputCss("headerCss") }}
    {% endif %}
    {% if (assets.collection("headerJs")) %}
        {{ assets.outputJs("headerJs") }}
    {% endif %}
    {% if (assets.collection("headerInlineJs")) %}
        {{ assets.outputInlineJs("headerInlineJs") }}
    {% endif %}
</head>
<body class="appear-animate">
{#<!-- Page Loader -->#}
<div class="page-loader">
    <div class="loader">Cargando...</div>
</div>
{#<!-- End Page Loader -->#}
{# Page Wrap #}
<div class="page" id="top">
    {{ content() }}
</div>
{#End Page Wrap #}
{#JS Individuales#}
{% if (assets.collection("footerJQuery")) %}
    {{ assets.outputJs("footerJQuery") }}
{% endif %}


{{ javascript_include('js/jquery-1.11.2.min.js') }}
{{ javascript_include('js/jquery.easing.1.3.js') }}
{{ javascript_include('js/bootstrap.min.js') }}
{{ javascript_include('js/SmoothScroll.js') }}
{{ javascript_include('js/jquery.scrollTo.min.js') }}
{{ javascript_include('js/jquery.localScroll.min.js') }}
{{ javascript_include('js/jquery.viewport.mini.js') }}
{{ javascript_include('js/jquery.countTo.js') }}
{{ javascript_include('js/jquery.appear.js') }}
{{ javascript_include('js/jquery.sticky.js') }}
{{ javascript_include('js/jquery.parallax-1.1.3.js') }}
{#{{ javascript_include('js/jquery.fitvids.js') }}#}
{{ javascript_include('js/owl.carousel.min.js') }}
{{ javascript_include('js/isotope.pkgd.min.js') }}
{{ javascript_include('js/imagesloaded.pkgd.min.js') }}
{{ javascript_include('js/jquery.magnific-popup.min.js') }}
{# Replace test API Key "AIzaSyAZsDkJFLS0b59q7cmW0EprwfcfUA8d9dg" with your own one below #}
{#**** You can get API Key here - https://developers.google.com/maps/documentation/javascript/get-api-key #}
{#<script type="text/javascript"#}
{#src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZsDkJFLS0b59q7cmW0EprwfcfUA8d9dg"></script>#}
{{ javascript_include('js/gmap3.min.js') }}
{{ javascript_include('js/wow.min.js') }}
{{ javascript_include('js/masonry.pkgd.min.js') }}
{{ javascript_include('js/jquery.simple-text-rotator.min.js') }}
{{ javascript_include('js/all.js?15') }}
{#{{ javascript_include('js/contact-form.js') }}#}
{{ javascript_include('js/jquery.ajaxchimp.min.js') }}
{{ javascript_include('js/tiltfx.js') }}

{{ javascript_include('rs-plugin/js/jquery.themepunch.tools.min.js') }}
{{ javascript_include('rs-plugin/js/jquery.themepunch.revolution.min.js') }}
{{ javascript_include('js/rev-slider.js?9') }}
{{ javascript_include('js/featherlight.js') }}
{{ javascript_include('js/jQuery.loadScroll.js') }}
{#{{ javascript_include('js/jquery.lazy.min.js') }}#}

<!-- [if lt IE 10]>
<script type="text/javascript" src="js/placeholder.js"></script><![endif]-->
{% if (assets.collection("footerJs")) %}
    {{ assets.outputJs("footerJs") }}
{% endif %}
{% if (assets.collection("footerInlineJs")) %}
    {{ assets.outputInlineJs("footerInlineJs") }}
{% endif %}
</body>
</html>
