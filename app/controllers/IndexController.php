<?php
use Libreria\PHPMailer\ServicioEmail as ServicioEmail;

//use Formulario\ContactoForm as ContactoForm;

class IndexController extends ControllerBase
{
    public function initialize()
    {
    }

    public function indexAction()
    {
        $this->view->setTemplateAfter('main');
        $this->assets->collection('footerJs')->addJs('js/enviarEmail.js?1');
        $this->assets->collection('footerInlineJs')
            ->addInlineJs(
                '     
                  $(window).load(function () {

                    // Page loader

                    $("body").imagesLoaded(function () {
                        $(".page-loader div").fadeOut();
                        $(".page-loader").delay(200).fadeOut("slow");
                    });
                });
        
                 '
            );
    }

    public function aboutAction()
    {
    }

    public function portfolioAction()
    {
        $this->view->setTemplateAfter('main_link');
//        $this->assets->collection('footerJs')
//            ->addJs('js/tiltfx.js');
        $this->assets->collection('footerInlineJs')
            ->addInlineJs(
                '
                $(".page-loader div").fadeOut();
                $(".page-loader").delay(200).fadeOut("slow");
                
                 /*LoAD SCroll*/
                $(function() {


                    $(\'img\').loadScroll(500);
            
                });
            '
            );

    }

    public function contactAction()
    {
    }


    public function enviarAction()
    {
        $this->view->disable();
        $recaptcha = $this->request->getPost('g-recaptcha-response');

        $secret = "6Ldz2I4UAAAAAGo12Q-ceL2PUSWR3P5CKyyWfM3y";

        $verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$recaptcha}");
        $captcha_success = json_decode($verify);
        if ($captcha_success->success == false) {

            // No eres un robot, continuamos con el envío del email
            $parametros = array(
                'nombre' => $this->request->getPost('nombre', ['string', 'upper']),
                'email' => $this->request->getPost('email', 'email'),
                'tipo' => $this->request->getPost('tipo', ['string', 'upper']),
                'telefono' => $this->request->getPost('telefono', ['int']),
                'mensaje' => $this->request->getPost('mensaje', ['string', 'upper'])
            );

            //check $_POST vars are set, exit if any missing
            if (!isset($parametros["nombre"]) || !isset($parametros["email"]) || !isset($parametros["mensaje"])) {
                $output = array('type' => 'error', 'text' => 'Todos los campos son requeridos');
                echo json_encode($output);
                return;
            }

            //Sanitize input data using PHP filter_var().
            $user_Name = filter_var($parametros["nombre"], FILTER_SANITIZE_STRING);
            $user_Tipo = filter_var($parametros["tipo"], FILTER_SANITIZE_STRING);
            $user_Telefono = filter_var($parametros["telefono"], FILTER_SANITIZE_NUMBER_INT);
            $user_Email = filter_var($parametros["email"], FILTER_SANITIZE_EMAIL);
            $user_Message = filter_var($parametros["mensaje"], FILTER_SANITIZE_STRING);

            $user_Message = str_replace("\&#39;", "'", $user_Message);
            $user_Message = str_replace("&#39;", "'", $user_Message);
            $cadena = "<p><b>Nombre Completo: </b> $user_Name</p>" .
                "<p><b>Email: </b> $user_Email</p>" .
                "<p><b>Evento: </b> $user_Tipo</p>" .
                "<p><b>Telefono: </b> $user_Telefono</p>" .
                "<p><b>Mensaje: </b> $user_Message </p>";
            //additional php validation
            if (strlen($user_Name) < 4) // If length is less than 4 it will throw an HTTP error.
            {
                $output = json_encode(array('type' => 'error', 'text' => 'El nombre es muy corto o está vacío'));
                die($output);
            }
            if (strlen($user_Tipo) < 4) // If length is less than 4 it will throw an HTTP error.
            {
                $output = json_encode(array('type' => 'error', 'text' => 'El tipo de evento es muy corto o está vacío> ' . $parametros['tipo']));
                die($output);
            }
            if (strlen($user_Telefono) < 4) // If length is less than 4 it will throw an HTTP error.
            {
                $output = json_encode(array('type' => 'error', 'text' => 'El télefono es muy corto o está vacío'));
                die($output);
            }
            if (!filter_var($user_Email, FILTER_VALIDATE_EMAIL)) //email validation
            {
                $output = json_encode(array('type' => 'error', 'text' => 'Ingrese un email válido'));
                die($output);
            }
            if (strlen($user_Message) < 5) //check emtpy message
            {
                $output = json_encode(array('type' => 'error', 'text' => 'El mensaje es demasiado corto, ingrese más detalles'));
                die($output);
            }
            if (strlen($user_Message) > 300) //check emtpy message
            {
                $output = json_encode(array('type' => 'error', 'text' => 'El mensaje es demasiado largo. No puede superar los 300 caracteres'));
                die($output);
            }


            $mailService = new ServicioEmail();
            $controlador = "index";
            $accion = "template";

            // el destino es capitancookeventos
            // copia al cliente
            // responder al cliente

            $destino = "capitancookeventos@gmail.com";
//            $destino = "dmunioz@imps.org";
            $responderA = array(
                array('correo' => $user_Email, 'nombre' => $user_Name)
            );
            $cc = array(
                $user_Email
            );
            $asunto = "Consulta web -  Evento: " . $user_Tipo;
            $imagenes =
                [
                    array('path' => 'images/general/logo.jpg', 'cid' => 'email_header', 'file' => 'email_header.jpg')
                ];


            $mailService->send($controlador, $accion, $cadena, $destino, $asunto, $imagenes, $responderA, $cc);
        } else {
            $output = array(
                'type' => 'error',
                'text' => 'No podemos asegurar que sea humano. No dejaremos que nos dominen los androides. Intentélo nuevamente.');
            echo json_encode($output);
            return;
        }
//        } else {
//            // Eres un robot!
//            $output = json_encode(array('type' => 'error', 'text' => 'La verificación del Captcha ha fallado, sospechamos que eres un robot. Intentalo nuevamente.'));
//            echo json_encode($output);
//            return;
//        }


    }

    public function templateAction()
    {

    }

}

