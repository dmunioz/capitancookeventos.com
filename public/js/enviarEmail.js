/**
 * Created by dmunioz on 11/02/2019.
 */

/* ---------------------------------------------
 Contact form
 --------------------------------------------- */
$(document).ready(function () {
    $("#submit_btn").click(function () {
        $("#submit_btn").hide();
        $("#submit_btn_espera").show();
        //get input field values
        var user_name = $("input[name=nombre]").val();
        var user_email = $("input[name=email]").val();
        var user_message = $("textarea[name=mensaje]").val();
        var user_tipo = $("select[name=tipo]").val();
        var user_telefono = $("input[name=telefono]").val();

        //simple validation at client"s end
        //we simply change border color to red if empty field using .css()
        var proceed = true;
        if (user_name == "") {
            $("input[name=nombre]").css("border-color", "#e41919");
            proceed = false;
        }
        if (user_email == "") {
            $("input[name=email]").css("border-color", "#e41919");
            proceed = false;
        }

        if (user_message == "") {
            $("textarea[name=mensaje]").css("border-color", "#e41919");
            proceed = false;
        }
        if (user_telefono == "") {
            $("input[name=telefono]").css("border-color", "#e41919");
            proceed = false;
        }
        if (user_tipo == "") {
            $("input[name=tipo]").css("border-color", "#e41919");
            proceed = false;
        }

        //everything looks good! proceed...
        if (proceed) {
            //data to be sent to server
            post_data = {
                "nombre": user_name,
                "email": user_email,
                "mensaje": user_message,
                "telefono": user_telefono,
                "tipo": user_tipo,
                captcha: grecaptcha.getResponse()

            };
            // console.log(">>>>>>>>>>>>>>>>>", post_data);
            $.ajax({
                type: "POST",
                url: "/index/enviar",
                dataType: "json",
                encode: true,
                data: post_data,
                async: false
            })
                .done(function (response) {
                    console.log("RESONSE ==>>", response);
                    //load json data from server and output message
                    if (response["type"] == "error") {
                        output = "<div class='error'>" + response["text"] + "</div>";
                    }
                    else {

                        output = "<div class='success'>" + response["text"] + "</div>";

                        //reset values in all input fields
                        $("#contact_form input").val("");
                        $("#contact_form textarea").val("");
                    }

                    $("#result").hide().html(output).slideDown();
                    $("#submit_btn").show();
                    $("#submit_btn_espera").hide();
                })
                .fail(function (data) {
                    console.log("ERROR ", data);
                    output = "<div class='success'> Ocurrio un error </div>";

                    //reset values in all input fields
                    $("#contact_form input").val("");
                    $("#contact_form textarea").val("");
                    $("#result").hide().html(output).slideDown();
                    $("#submit_btn").show();
                    $("#submit_btn_espera").hide();
                });

        }

        return false;
    });

    //reset previously set border colors and hide all message on .keyup()
    $("#contact_form input, #contact_form textarea").keyup(function () {
        $("#contact_form input, #contact_form textarea").css("border-color", "");
        $("#result").slideUp();
    });

});

